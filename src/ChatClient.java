import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ChatClient {
	static Socket socket;
	static InputStream is;
	static int port;
	static ChatWindow cw;
	static String username;

	public static void main(String[] args) throws UnknownHostException, IOException {
		// om det går att starta i terminalen:

		if (args.length != 2) {
			System.out.println("Syntax: java ChatClient machine port");
			System.exit(1);
		}
		
		System.out.println("Pick a username: ");
		Scanner scan = new Scanner(System.in);
		
		if (scan.hasNext()) {
			username = scan.nextLine();

		}
		
		port = Integer.parseInt(args[1]);
		socket = new Socket(args[0], port);
		is = socket.getInputStream(); // vill ha den här!!

		cw = new ChatWindow(0, 0, username, socket);
		cw.show();
		
		scan.close();

	}

	public String getUsername() {
		return username;
	}
}
