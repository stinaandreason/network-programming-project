import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class ServerInput extends Thread {

	Monitor m;
	Socket socket;
	InputStream is;
	OutputStream os;
	ServerOutput rd;
	String username;

	public ServerInput(Monitor m, Socket socket, ServerOutput rd, InputStream is) {
		this.m = m;
		this.socket = socket;
		this.rd = rd;
		this.is = is;
		username = "user";
	}

	public void run() {
		rd.start();
		System.out.println(socket.getInetAddress().toString() + " Port: " + socket.getPort() + '\n');
		try {
			os = socket.getOutputStream();
			while (!socket.isClosed()) {
				String s = new String();
				int b = is.read();
				s += (char) b;
				while (b != '\n') {
					b = is.read();
					s += (char) b;
				}

				if (!s.isEmpty()) {
					m.deposit(s);
				}
			}
			socket.close();
			m.removeFromList(this);

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("error");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void print(String s) throws IOException {
		os.write(s.getBytes());
	}

	public String getUser() {
		return username;
	}

}
