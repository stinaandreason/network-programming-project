	import java.util.ArrayList;

	public class Monitor {
		private String message;
		ArrayList<ServerInput> list;
		private ListWindow lw;
		
		public Monitor(){
			message = "";
			list = new ArrayList<ServerInput>();
			lw = new ListWindow(600, 0, "online");
			lw.show();
		}
		
		public synchronized void deposit(String s) throws InterruptedException{
			while(!message.equals("")){
				wait();
			}
			message = s;
			System.out.println(message);
			notifyAll();
		}
		
		public synchronized String remove() throws InterruptedException{
			while(message.isEmpty()){
				wait();
			}
			String temp = message;
			message = "";

			notifyAll();
			return temp;
		}
		
		public boolean isMessage(){
			if(message.equals("")){
				return false;
			} else {
				return true;
			}
		}
		
		public void add(ServerInput p){
			list.add(p);
			lw.add(p.getUser());
		}
		
		public ArrayList<ServerInput> getList(){
			return list;
		}
		
		public void removeFromList(ServerInput p){
			list.remove(p);
			lw.clear();
			for(ServerInput s : list){
				lw.add(s.getUser());
			}
		}
	}

