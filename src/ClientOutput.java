import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class ClientOutput extends Thread {
	Socket socket;
	OutputStream os;
	Scanner scan;
	ChatWindow cw;

	public ClientOutput(Socket socket, ChatWindow cw) throws IOException {
		this.socket = socket;
		os = socket.getOutputStream();
		this.cw = cw;
	}

	public void run() {
		while (!socket.isClosed()) {
			scan = new Scanner(System.in);

			if (scan.hasNextLine() || scan.next().equals("\n")) {

				String s = scan.nextLine() + "\n";

				try {
					os.write(s.getBytes()); // klarar inte åäö
					os.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}
			}

		}
		
		try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		scan.close();
		return;
	}

}
