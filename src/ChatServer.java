	import java.io.IOException;
	import java.net.ServerSocket;
	import java.net.Socket;
	import java.util.Vector;

	public class ChatServer {
		static Vector<ServerInput> list;
		static Monitor m;
		static ServerOutput reader;

		public static void main(String[] args) throws IOException {
			
			
			m = new Monitor();
			list = new Vector<ServerInput>();

			ServerSocket ss = new ServerSocket(30000);

			while (!ss.isClosed()) {
				Socket s = ss.accept();
				ServerInput p = new ServerInput(m, s, new ServerOutput(m), s.getInputStream());
				m.add(p);
				p.start();
				//lw.add(p.getUser());
				
			}
			ss.close();

		}
		public Monitor getMonitor(){
			return m;
		}

}
